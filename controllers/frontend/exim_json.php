<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (Registry::get('addons.exim_json.status') != 'A') {
    fn_echo('ADDON DISABLED');
    exit;
}

if (!empty($_SERVER['PHP_AUTH_USER'])) {
    $_data['user_login'] = $_SERVER['PHP_AUTH_USER'];
} else {
    fn_exim_json_auth_error('EMPTY_USER_1C');
    exit;
}

list($status, $user_data, $user_login, $password, $salt) = fn_auth_routines($_data, array());

if ($user_login != $_SERVER['PHP_AUTH_USER'] || $user_data['password'] != fn_generate_salted_password($_SERVER['PHP_AUTH_PW'], $salt)) {
    fn_exim_json_auth_error('WRONG_KEY_1C');
    exit;
}

if (!fn_exim_json_allowed_access($user_data)) {
    fn_echo('ACCESS DENIED');
    exit;
}

$company_id = 0;
if (PRODUCT_EDITION == 'ULTIMATE') {
    if (Registry::get('runtime.simple_ultimate')) {
        $company_id = Registry::get('runtime.forced_company_id');
    } else {
        if ($user_data['company_id'] == 0) {
            fn_echo('SHOP IS NOT SIMPLE');
            exit;
        } else {
            $company_id = $user_data['company_id'];
            Registry::set('runtime.company_id', $company_id);
        }
    }
} elseif ($user_data['user_type'] == 'V') {
    if ($user_data['company_id'] == 0) {
        fn_echo('SHOP IS NOT SIMPLE');
        exit;
    } else {
        $company_id = $user_data['company_id'];
        Registry::set('runtime.company_id', $company_id);
    }
} else {
    Registry::set('runtime.company_id', $company_id);
}

$type = $mode = '';
if (isset($_REQUEST['type'])) {
    $type = $_REQUEST['type'];
}
if (isset($_REQUEST['mode'])) {
    $mode = $_REQUEST['mode'];
}
if (isset($_REQUEST['images'])) {
    $img_url = $_REQUEST['images'];
} else {
    $img_url = '';
}
if (isset($_REQUEST['service_exchange'])) {
    $service_exchange = $_REQUEST['service_exchange'];
}

$filename = (!empty($_REQUEST['filename'])) ? fn_basename($_REQUEST['filename']) : '';
$lang_code = Registry::get('addons.rus_exim_1c.exim_1c_lang');
if (empty($lang_code)) {
    $lang_code = CART_LANGUAGE;
}

$save_to_temp_products = (Registry::get('addons.exim_json.exim_json_save_product_imports') == 'Y')?true:false;
$save_to_temp_users = (Registry::get('addons.exim_json.exim_json_save_users_imports') == 'Y')?true:false;
$save_to_temp_orders = (Registry::get('addons.exim_json.exim_json_save_orders_imports') == 'Y')?true:false;
$import_statuses = Registry::get('addons.rus_exim_1c.exim_1c_import_statuses');

if ($type == 'catalog') {
    if ($mode == 'checkauth') {
        fn_exim_json_checkauth();
    } elseif ($mode == 'init') {
        fn_exim_json_init();
    } elseif ($mode == 'file') {
        if (fn_exim_json_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }
        fn_echo("success");
    } elseif ($mode == 'json_prodimport') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_json_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        if ($save_to_temp_products) {
            fn_exim_json_save_to_tmp_file($filename, $mode);
        }
        Registry::set('rus_exim_json.images_successfull', false);
        if ($img_url) {
            if (filter_var($img_url, FILTER_VALIDATE_URL)) {
                fn_exim_json_download_file($img_url, true);   
            } else {
                fn_echo("failureimgs");
            }    
        }
        fn_exim_json_import_json($json, $user_data, $company_id, $mode, $lang_code);
    } elseif ($mode == 'json_users') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_json_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        if ($save_to_temp_users) {
            fn_exim_json_save_to_tmp_file($filename, $mode);
        }
        fn_exim_json_import_users($json, $user_data, $company_id, $lang_code);
    } elseif ($mode == 'json_currency') {
        $json = fn_exim_json_get_json($filename);
        if ($json === false) {
            fn_echo("file_failure");
            exit;
        }
        fn_exim_json_import_json($json, $user_data, $company_id, $mode, $lang_code);
    } elseif ($mode == 'images') {
        $url = fn_get_contents('php://input');
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            fn_exim_json_download_file($url, true);   
        } else {
            fn_echo("failure");
        }
    } elseif ($mode == 'testprm') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_json_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        if ($save_to_temp_products) {
            fn_exim_json_save_to_tmp_file($filename, $mode);
        }
        Registry::set('rus_exim_json.images_successfull', false);
        if ($img_url) {
            if (filter_var($img_url, FILTER_VALIDATE_URL)) {
                fn_exim_json_download_file($img_url, true);   
            } else {
                fn_echo("failureimgs");
            }    
        }
        fn_exim_json_import_json($json, $user_data, $company_id, "json_prodimport", $lang_code,1);
    }
} elseif (($type == 'sale') && ($user_data['user_type'] != 'V') && (Registry::get('addons.rus_exim_1c.exim_1c_check_prices') != 'Y')) {
    if ($mode == 'checkauth') {
        fn_exim_json_checkauth();
    } elseif ($mode == 'init') {
        fn_exim_json_init();
    //export 1C orders to CS-Cart
    } elseif ($mode == 'file') {
        if (fn_exim_json_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }

        if (($import_statuses == 'Y') && (strpos($filename, 'orders') == 0)) {
            $xml = fn_exim_json_get_xml($filename);
            if ($xml === false) {
                fn_echo("failure");
                exit;
            }
            fn_exim_json_import_orders($xml, $user_data, $company_id, $lang_code);
        }

        fn_echo("success");
    //export CS-Cart orders to 1C
    } elseif ($mode == 'orders_import') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_json_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        if ($save_to_temp_orders) {
            fn_exim_json_save_to_tmp_file($filename, $mode);
        }
        fn_exim_json_importjson_orders($json, $user_data, $company_id, $lang_code);
    //export CS-Cart orders to 1C
    } elseif ($mode == 'query') {
        fn_exim_json_export_orders($company_id, $lang_code);
    } elseif ($mode == 'success') {
        fn_echo("success");
    } elseif ($mode == 'orders_export') {
        fn_exim_json_json_export_orders($company_id, $lang_code);
    } elseif ($mode == 'orders_export_test') {
        fn_exim_json_json_export_orders($company_id, $lang_code,1);
    } elseif ($mode == 'file') {
        if (fn_exim_json_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }
        fn_echo("success");
    }

}

exit;
