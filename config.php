<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

//GTR

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

/*Common settings for json imports*/
define('SCHEMA_KEY', 'schema_update');
define('IMPORT_FIELD', 'ЗагружатьНа_B2B');
define('REMOVE_FIELD', 'УбратьС_B2B');
const JSON_ACT_MAPPING = [
						'active' => 'A',
						'hidden' => 'H',
						'disabled' => 'D'
						];
/*Settings for currencies import*/
const JSON_CURRENCY_CODES = ['840' => 'USD', '643' => 'RUR'];
define('JSON_CURRENCY_KEY', 'nal');
const JSON_PRICES_TO_RECALC = array();

/*Settings for products import*/
const RES_FIELDS = [
				"наличие" => "amount",
				"описание" => "short_description",
				"код_товара" => "external_id",
				"артикул" => "product_code",
				"qimage" => "exim_json_images_q"
				];
const DEFAULT_PRODUCT_VALUES = [
							'price' => '0.00',
							'lower_limit' => 1,
							'details_layout' => 'default',
							'out_of_stock_actions' => 'B',
							'zero_price_action' => 'R',
							'feature_comparison' => 'Y'
							];
const PROD_EXEMPT_FIELDS = ["производитель", "гарантия", "подзаказ", "наличие", "описание", "поиск", "код_товара", "витрина", "название", "изображения", "опт_1",
						"модель", "артикул", "опт_2", "опт_3", "розница", "ожидаемое", "qimage", "СрокДоставки","УбратьС_B2B", "код_товара_пл", "Товар_DDP", "foto", "розница_DDP",
						"название_пл", "ожидаемое_ПЛ", "наличие_ПЛ", "ЗагружатьНа_B2B", "опт_3_DDP", "подзаказ_ПЛ", "артикул_пл", "опт_2_DDP", "ЗагружатьНа_PL", "УбратьС_PL",
						"опт_1_DDP", "prices", "prices_DDP"];
const AVAIL_FIELDS = ["наличие", "ожидаемое", "витрина", "подзаказ", "СрокДоставки"];
define('IMGS_FIELD', 'foto');
define('REMOTE_IMGS_FIELD', 'foto');
define('LOCAL_IMGS_FIELD', 'изображения');
define('AMOUNT_FIELD', "наличие");
define('NAME_FIELD', "название");
define('CODE_FIELD', "код_товара");
define('SHOWCASE_FIELD', "витрина");
define('FOR_ORDER_FIELD', "подзаказ");
define('EXPECTED_FIELD', "ожидаемое");
define('AVAIL_SINCE_FIELD', "СрокДоставки");
define('MANUFACTURER_FIELD', "производитель");
define('BASE_PRICE_FIELD', "опт_2");
define('BASE_PRICE_FIELD_CURRENCY', "643");
define('PRICES_FIELD', "prices");
define('JSON_IMG_PREFIX', 'D:\\Pic\\');
/*Settings for categories import*/
const CAT_EXEMPT_FIELDS = ["date","schema_update","Prise_update"];
const CAT_RESERVED_FIELDS = ["код_категории","код_родитель","видимость_категории","родитель","порядок_категории","ЗагружатьНа_PL",
	"Наименование_категории_PL","ЗагружатьНа_B2B","УбратьС_B2B","УбратьС_PL"];
define('CAT_DISABLED_FIELD', 'видимость_категории');
define('CAT_NAME_FIELD', '');
define('CAT_CODE_FIELD', 'код_категории');
define('CAT_PARCODE_FIELD', 'код_родитель');
/*Settings for features import*/
define('MAN_FEATURE_ID', 18);
define('MAN_FEATURE_1C_CODE', '1000');
define('CAT_FEATURE_1C_CODE', '1001');
define('FEAT_CODE_FIELD', 'код_характеристики');
/*Settings for filters creation*/
define('FILTER_CREATION_FIELD', 'в_фильтре');
define('FILTER_ORDER_FIELD', 'порядок');
/*Settings for usergroups import*/
define('USERGROUP_FIELD', "Prise_update");
/*Settings for users import*/
const RES_USERS_FIELDS = [
						"external_id" => "ид",
						"firstname" => "НазваниеКонтрагента",
						"email" => "mail",
						"s_firstname" => "НазваниеКонтрагента",
						"b_firstname" => "НазваниеКонтрагента",
						"company" => "НазваниеКонтрагента",
						"b2b_overall_debt" => "общий_долг",
						"b2b_contract_currency" => "ВалютаДоговора",
						"b2b_contract_number" => "НомерДоговора",
						"b2b_contract_name" => "Договор",
						"b2b_expected_payment" => "к оплате",
						"b2b_credit_value" => "кредит",
						"b2b_manager" => "менеджер",
						"b2b_manager_skype" => "Skype_менеджера",
						"b2b_manager_phone" => "телефон_менеджера",
						"b2b_manager_icq" => "ICQ_менеджера",
						"b2b_manager_email" => "EMail_менеджера",
						"b2b_avail_for_shipment" => "ВозможностьОтгрузки"
						];
const DEFAULT_USERS_VALUES = [
						'user_type' => 'C',
						'is_root' => 'N',
						'tax_exempt' => 'N',
							];
define('USER_EXTID_FIELD', "ид");
define('USER_EMAIL_FIELD', "mail");
define('USER_PASS_FIELD', "password");
define('USER_USRGRP_FIELD', "ТипЦен");
define('USER_BLOCK_FIELD', "Заблокирован");
define('USER_DATA_FIELD', "data");
/*Settings for orders export*/
const ORDER_EXPORT_MAPPING_FIELDS = [
								'email' => 'user_email',
								'phone' => 'user_phone',
								'notes' => 'comment'
									];
const USER_EXPORT_MAPPING_FIELDS = [
								];
const PRODUCT_EXPORT_MAPPING_FIELDS = [
								'external_id' => 'code'
									];
/*Settings for samba mount*/
define('REMOUNT_FILE', '/var/www/cscart/run/remount_file');
define('SAMBA_FLAG', '/var/www/cscart/images_mnt/flagfile');
define('MOUNTPOINT_1C_remote', '/var/www/cscart/images_mnt/');
define('MOUNTPOINT_1C_local', '/var/www/cscart/images_1C/');


