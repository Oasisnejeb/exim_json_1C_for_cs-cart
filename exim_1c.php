<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (Registry::get('addons.rus_exim_1c.status') != 'A') {
    fn_echo('ADDON DISABLED');
    exit;
}

if (!empty($_SERVER['PHP_AUTH_USER'])) {
    $_data['user_login'] = $_SERVER['PHP_AUTH_USER'];
} else {
    fn_exim_1c_auth_error('EMPTY_USER_1C');
    exit;
}

list($status, $user_data, $user_login, $password, $salt) = fn_auth_routines($_data, array());

if ($user_login != $_SERVER['PHP_AUTH_USER'] || $user_data['password'] != fn_generate_salted_password($_SERVER['PHP_AUTH_PW'], $salt)) {
    fn_exim_1c_auth_error('WRONG_KEY_1C');
    exit;
}

if (!fn_rus_exim_1c_allowed_access($user_data)) {
    fn_echo('ACCESS DENIED');
    exit;
}

$company_id = 0;
if (PRODUCT_EDITION == 'ULTIMATE') {
    if (Registry::get('runtime.simple_ultimate')) {
        $company_id = Registry::get('runtime.forced_company_id');
    } else {
        if ($user_data['company_id'] == 0) {
            fn_echo('SHOP IS NOT SIMPLE');
            exit;
        } else {
            $company_id = $user_data['company_id'];
            Registry::set('runtime.company_id', $company_id);
        }
    }
} elseif ($user_data['user_type'] == 'V') {
    if ($user_data['company_id'] == 0) {
        fn_echo('SHOP IS NOT SIMPLE');
        exit;
    } else {
        $company_id = $user_data['company_id'];
        Registry::set('runtime.company_id', $company_id);
    }
} else {
    Registry::set('runtime.company_id', $company_id);
}

$type = $mode = '';
if (isset($_REQUEST['type'])) {
    $type = $_REQUEST['type'];
}
if (isset($_REQUEST['mode'])) {
    $mode = $_REQUEST['mode'];
}
if (isset($_REQUEST['service_exchange'])) {
    $service_exchange = $_REQUEST['service_exchange'];
}

$filename = (!empty($_REQUEST['filename'])) ? fn_basename($_REQUEST['filename']) : '';
$lang_code = Registry::get('addons.rus_exim_1c.exim_1c_lang');
if (empty($lang_code)) {
    $lang_code = CART_LANGUAGE;
}

$import_statuses = Registry::get('addons.rus_exim_1c.exim_1c_import_statuses');

if ($type == 'catalog') {
    if ($mode == 'checkauth') {
        fn_exim_1c_checkauth();
    } elseif ($mode == 'init') {
        fn_exim_1c_init();
    } elseif ($mode == 'file') {
        if (fn_exim_1c_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }
        fn_echo("success\n");
    } elseif ($mode == 'import') {
        $fileinfo = pathinfo($filename);
        $xml = fn_exim_1c_get_xml($filename);
        if ($xml === false) {
            fn_echo("failure");
            exit;
        }
        if (strpos($fileinfo['filename'], 'import') == 0) {
            if (Registry::get('addons.rus_exim_1c.exim_1c_import_products') != 'not_import') {
                fn_exim_1c_import($xml, $user_data, $company_id, $lang_code);
            }
        }
        if (strpos($fileinfo['filename'], 'offers') == 0) {
            if (Registry::get('addons.rus_exim_1c.exim_1c_only_import_offers') == 'Y') {
                fn_exim_1c_offers($xml, $company_id, $lang_code);
            }
        }

        fn_echo("success\n");
    } elseif ($mode == 'json_prodimport') {
          $fileinfo = pathinfo($filename);
        $json = fn_exim_1c_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        $bkpfilepath =TMP_FOLDER .  date('d-m-Y_H-i-s\.\j\s\o\n');
    $bkpfile = @fopen($bkpfilepath, 'w');
    list($dir_1c, $dir_1c_url, $dir_1c_images) = fn_rus_exim_1c_get_dir_1c();
//  fn_print_r($dir_1c . $filename);
    fwrite($bkpfile, fn_get_contents($dir_1c . $filename));
    fclose($bkpfile);
    fn_exim_1c_importjson_cat($json, $user_data, $company_id, $lang_code);
    fn_exim_1c_importjson_prod($json, $user_data, $company_id, $lang_code);
    } elseif ($mode == 'json_catimport') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_1c_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        fn_exim_1c_importjson_cat($json, $user_data, $company_id, $lang_code);
    } elseif ($mode == 'json_currency') {
        $json = fn_exim_1c_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        $currency = fn_exim_1c_importjson_currency($json);
    if ($currency === false) {
        fn_echo("failure");
    } else {
        fn_echo("success\n");
    }
    } elseif ($mode == 'testprm') {
        fn_exim_1c_check_yml_readiness();
//    $storage = fn_exim_1c_check_storage();
//  fn_print_r(get_declared_classes());
//  $bkpfile = date('d-m-Y_H-i\.\j\s\o\n');
    fn_echo($storage);
    } elseif ($mode == 'json_prodimport_test') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_1c_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        $bkpfilepath =TMP_FOLDER .  date('d-m-Y_H-i-s\.\j\s\o\n');
        $bkpfile = @fopen($bkpfilepath, 'w');
        fwrite($bkpfile, fn_get_contents('php://input'));
        fclose($bkpfile);
        fn_exim_1c_importjson_cat($json, $user_data, $company_id, $lang_code, 1);
        fn_exim_1c_importjson_prod($json, $user_data, $company_id, $lang_code, 1);
    }
} elseif (($type == 'sale') && ($user_data['user_type'] != 'V') && (Registry::get('addons.rus_exim_1c.exim_1c_check_prices') != 'Y')) {
    if ($mode == 'checkauth') {
        fn_exim_1c_checkauth();
    } elseif ($mode == 'init') {
        fn_exim_1c_init();
    //export 1C orders to CS-Cart
    } elseif ($mode == 'file') {
        if (fn_exim_1c_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }

        if (($import_statuses == 'Y') && (strpos($filename, 'orders') == 0)) {
            $xml = fn_exim_1c_get_xml($filename);
            if ($xml === false) {
                fn_echo("failure");
                exit;
            }
            fn_exim_1c_import_orders($xml, $user_data, $company_id, $lang_code);
        }

        fn_echo("success\n");
    //export CS-Cart orders to 1C
    } elseif ($mode == 'orders_import') {
        $fileinfo = pathinfo($filename);
        $json = fn_exim_1c_get_json($filename);
        if ($json === false) {
            fn_echo("failure");
            exit;
        }
        $bkpfilepath =TMP_FOLDER .  "orders-" . date('d-m-Y_H-i-s\.\j\s\o\n');
        $bkpfile = @fopen($bkpfilepath, 'w');
        fwrite($bkpfile, fn_get_contents('php://input'));
        fclose($bkpfile);
        fn_exim_1c_importjson_orders($json, $user_data, $company_id, $lang_code);
    //export CS-Cart orders to 1C
    } elseif ($mode == 'query') {
        fn_exim_1c_export_orders($company_id, $lang_code);
    } elseif ($mode == 'success') {
        fn_echo("success");
    } elseif ($mode == 'orders_export') {
        fn_exim_1c_json_export_orders($company_id, $lang_code);
    } elseif ($mode == 'orders_export_test') {
        fn_exim_1c_json_export_orders($company_id, $lang_code,1);
    } elseif ($mode == 'file') {
        if (fn_exim_1c_get_external_file($filename) === false) {
            fn_echo("failure");
            exit;
        }
        fn_echo("success\n");
    }

}

exit;
